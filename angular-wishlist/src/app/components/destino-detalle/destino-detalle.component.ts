import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client.model';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
     DestinosApiClient
  ]
})
export class DestinoDetalleComponent implements OnInit {
  destino: DestinoViaje;

  style = {
    sources:{
      world:{
        type: 'geojson',
        data: 'https://d2ad6b4ur7yvpq.cloudfront.net/naturalearth-3.3.0/ne_50m_urban_areas.geojson'
      }
  },
  version: 8,
  layers: [{
    'id': 'countries',
    'type': 'fill',
    'source': 'world',
    'layout': {},
    'paint': {
      'fill-color': '#6F788A'
    }
  }]
};
  constructor(private route: ActivatedRoute, private DestinosApiClient:DestinosApiClient) { }

  ngOnInit(){
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.DestinosApiClient.getById(id);
  };

};
